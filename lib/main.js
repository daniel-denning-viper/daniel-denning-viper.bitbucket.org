/*******************************************************************************
 *	Main.js		Main javascript routines
 *			T.Barnaby,	BEAM Ltd,	2010-07-02
 *******************************************************************************
 */

function reloadPage(){
	window.location.reload(true);
}

function reloadPageTrigger(time)
{
    setTimeout("reloadPage()", time);
}

function dataSet(str){
	var	data = {};

//	document.getElementById("status").innerHTML += "Set\n";

	// Parse string
	str = str.replace("/\+/g", ' ');
	var lines = str.split("\n");

	args = lines[0].split('&');
	for(var i = 0; i < args.length; i++){
		var	nv = args[i].split('=');
		var	name = decodeURIComponent(nv[0]);
		var	value = (nv.length == 2) ? decodeURIComponent(nv[1]) : name;

//		document.writeln("Set: -" + name + "- Value: " + value + "<br>\n");
		if(document.getElementById(name + "_checkbox")){
			document.getElementById(name).value = value;
			document.getElementById(name + "_checkbox").checked = parseInt(value);
		}
		else if(document.getElementById(name + "_led")){
			if(parseInt(value))
				document.getElementById(name + "_led").src = "images/LedRed.png";
			else
				document.getElementById(name + "_led").src = "images/LedOff.png";
		}
		else if(document.getElementById(name)){
			var obj = document.getElementById(name);

			obj.innerHTML = value;
				
//			if(obj.hasOwnProperty("value"))
//				obj.value = value;
//			else
//				obj.innerHTML = value;
		}
	}

/*	
	args = lines[1].split('&');
	for(var i = 0; i < args.length; i++){
		var	nv = args[i].split('=');
		var	name = decodeURIComponent(nv[0]);
		var	value = (nv.length == 2) ? decodeURIComponent(nv[1]) : name;

		if(document.getElementById(name))
			document.getElementById(name).className = value;
	}
*/
}

var autoUpdate = 1;
		
function fetchDataAndSet(url){
	xmlhttp=new XMLHttpRequest();
	xmlhttp.open('GET.html', url, true);
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState==4 && xmlhttp.status == 200){
			dataSet(xmlhttp.responseText);
		}
	};
	xmlhttp.send("");
}

function fetchDataAndSetLoop(url, repeatTimeMs){
	var running = false;

	if(!running){
		running = true;
		function run(){
			fetchDataAndSet(url);
		}
		setInterval(run, repeatTimeMs);
	}
};


function str_repeat(i, m) {
	for (var o = []; m > 0; o[--m] = i);
	return o.join('');
}

function sprintf() {
	var i = 0, a, f = arguments[i++], o = [], m, p, c, x, s = '';
	while (f) {
		if (m = /^[^\x25]+/.exec(f)) {
			o.push(m[0]);
		}
		else if (m = /^\x25{2}/.exec(f)) {
			o.push('%');
		}
		else if (m = /^\x25(?:(\d+)\$)?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(f)) {
			if (((a = arguments[m[1] || i++]) == null) || (a == undefined)) {
				throw('Too few arguments.');
			}
			if (/[^s]/.test(m[7]) && (typeof(a) != 'number')) {
				throw('Expecting number but found ' + typeof(a));
			}
			switch (m[7]) {
				case 'b': a = a.toString(2); break;
				case 'c': a = String.fromCharCode(a); break;
				case 'd': a = parseInt(a); break;
				case 'e': a = m[6] ? a.toExponential(m[6]) : a.toExponential(); break;
				case 'f': a = m[6] ? parseFloat(a).toFixed(m[6]) : parseFloat(a); break;
				case 'o': a = a.toString(8); break;
				case 's': a = ((a = String(a)) && m[6] ? a.substring(0, m[6]) : a); break;
				case 'u': a = Math.abs(a); break;
				case 'x': a = a.toString(16); break;
				case 'X': a = a.toString(16).toUpperCase(); break;
			}
			a = (/[def]/.test(m[7]) && m[2] && a >= 0 ? '+'+ a : a);
			c = m[3] ? m[3] == '0' ? '0' : m[3].charAt(1) : ' ';
			x = m[5] - String(a).length - s.length;
			p = m[5] ? str_repeat(c, x) : '';
			o.push(s + (m[4] ? a + p : p + a));
		}
		else {
			throw('Huh ?!');
		}
		f = f.substring(m[0].length);
	}
	return o.join('');
}

function dateUtcString(){
	var	d = new Date();
	
	return sprintf("%04d-%02d-%02dT%02d:%02d:%02d", d.getUTCFullYear(), d.getUTCMonth() + 1, d.getUTCDate(), d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds());
}


function progress(url){
	var running = false;

	if(!running){
		running = true;
		var v = 0;
		var state = 1;
		var inter = null;
		function run(){
			function fetchValue(){
				xmlhttp=new XMLHttpRequest();
				xmlhttp.open('GET', url, true);
				xmlhttp.onreadystatechange = function(){
					if(xmlhttp.readyState==4 && xmlhttp.status == 200){
						xml = xmlhttp.responseXML;
						state = xml.getElementsByTagName("state")[0].childNodes[0].nodeValue;
						stateName = xml.getElementsByTagName("stateName")[0].childNodes[0].nodeValue;
						progressValue = xml.getElementsByTagName("progressValue")[0].childNodes[0].nodeValue;
						progressLog = xml.getElementsByTagName("log")[0].childNodes[0].nodeValue;
						if(obj = document.getElementById('progressStateName'))
							obj.innerHTML = stateName;
						if(obj = document.getElementById('progressBar')){
							obj.style.width = (progressValue+"%");
							obj.innerHTML = (progressValue+"%");
						}
						if(obj = document.getElementById('progressLog'))
							obj.innerHTML = decodeURIComponent(progressLog);
					}
				};
				xmlhttp.send();
			}
			fetchValue();
			if(state != 1){
				clearInterval(inter);
				running = false;
			}
		}
		inter = setInterval(run, 1000);
	}
};

function flashBackgroundToggle(id, color1, color2){
	obj = document.getElementById(id);
	if(obj.style.backgroundColor == color1)
		obj.style.backgroundColor = color2;
	else
		obj.style.backgroundColor = color1;
}	

function flashBackground(id, color1, color2){
	function run(){
		flashBackgroundToggle(id, color1, color2);
	}

	setInterval(run, 1000);
}
